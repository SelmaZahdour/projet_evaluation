#ZAHDOUR Selma
# Projet Evaluation r, SQL, Shiny

############ PARTIE 1 : R

# Chargement des packages

library(dplyr)
library(tidyr)
library(ggplot2)
library(writexl)


#Chargement des données

rea_ped=read.csv2('C:/Users/33766/data_management/projet_evaluation/rea_ped.csv')

#1 - Passage des noms des colonnes en minuscule

colnames(rea_ped) <- tolower(colnames(rea_ped))

#2 - Création d'une variable age_annee qui calcule l'âge en années

rea_ped <- rea_ped %>%
  mutate(age_annee = age / 12)


#3 - Création d'une nouvelle variable "age_classe", qui sépare les âges en modalités

rea_ped$age_classe <- cut(rea_ped$age,
                          breaks = c(0, 6, 24, 60, 144, Inf),
                          labels = c("[0-6 mois]", "]6 mois-2 ans]", "]2 ans-5 ans]", "]5 ans-12 ans]", "]>12 ans]"),
                          include.lowest = TRUE)



#4 - Ajout d'une variable binaire : insuffisance cardiaque

rea_ped <- rea_ped %>%
  mutate(insuffisance_cardiaque = ifelse(bnp_h0 > 500 | bnp_h12 > 500, 1, 0))


#5 - Construction d'un dataframe medicament_row

rea_ped_meds <- rea_ped %>%
  select(id, starts_with("nad"), starts_with("adre"), starts_with("milri"), starts_with("lasilix"), starts_with("transfu_cgr"), starts_with("transfu_plq"), starts_with("transfu_pfc"), starts_with("transfu_fibri"))

medicament_row <- rea_ped_meds %>%
  gather(key = "administration_heure", value = "valeur", -id) %>%
  separate(administration_heure, into = c("administration", "heure"), sep = "_", convert = TRUE) %>%
  select(id, administration, heure, valeur)


#6 - Construction d'un dataframe mesure_row

mesure_row <- rea_ped %>%
  select(id, starts_with("ph_a"), starts_with("pco2_a"), starts_with("po2_a"), starts_with("lactates_a"), starts_with("ph_v"), starts_with("pco2_v"), starts_with("po2_v"), starts_with("lactates_v"),
         starts_with("scvo2"), starts_with("pvc"), starts_with("diurese"), starts_with("nirs_r"), starts_with("nirs_c"), starts_with("temp"), starts_with("tropo"), starts_with("bnp"), starts_with("hb"), starts_with("plq"), starts_with("fibri")) %>%
  gather(key = "Mesure_Heure", value = "Valeur", -id) %>%
  separate(Mesure_Heure, into = c("Mesure", "Heure"), sep = "_h") %>%
  mutate(Heure = as.numeric(Heure)) 

#7 - construction d'un dataframe medicament


cols_medicaments <- c("id", grep("nad_h|adre_h|milri_h|lasilix_h", names(rea_ped), value = TRUE))

medicament <- rea_ped %>%
  select(all_of(cols_medicaments)) %>%
  mutate(across(-id, ~ ifelse(. > 0, 1, 0))) %>% 
  group_by(id) %>%
  summarise(across(everything(), ~ max(.), .names = "max_{.col}"),
            across(everything(), ~ min(.), .names = "min_{.col}")) %>%
  ungroup()

medicament <- medicament %>%
  left_join(rea_ped %>% select(id, transfu_cgr, transfu_plq, transfu_pfc, transfu_fibri), by = "id") %>%
  mutate(across(c(transfu_cgr, transfu_plq, transfu_pfc, transfu_fibri), ~ ifelse(. > 0, 1, 0)))


medicament <- medicament %>%
  mutate(nad_adre_milri = ifelse(max_nad_h0:max_nad_h12 + max_adre_h0:max_adre_h12 + max_milri_h0:max_milri_h12 > 0, 1, 0),
         transf_pfc_plq_fibri = ifelse(transfu_pfc + transfu_plq + transfu_fibri > 0, 1, 0))

#8 - Construction d'un dataframe mesure


mesures2 <- c("pha_a", "pco2_a", "lactate_a", "scvo2")

mesure <- rea_ped%>%
  select(id, starts_with(mesures2))
group_by(id) %>%
  summarise(
    mesure_min = min(across(starts_with(mesures2))),
    mesure_max = max(across(starts_with(mesures2)))
  )

#9 Fusion des 3 dataframes, rea_ped, medicament et mesure

# Fusion des dataframes rea_ped et medicament par la colonne id
rea_ped_medicament_mesure <- left_join(rea_ped, medicament, by = "id")

# Fusion du résultat avec le dataframe mesure par la colonne id
rea_ped_medicament_mesure <- left_join(rea_ped_medicament_mesure, mesure, by = "id")

# 10 - Enregistrement du fichier


write_xlsx(rea_ped_medicament_mesure, "C:/Users/33766/data_management/projet_evaluation/rea_ped_medicament_mesure.xlsx")


# on représente l’âge, le poids, le sexe et l’insuffisance cardiaque en fonction des années
##########
ggplot(rea_ped_medicament_mesure, aes(x = ddc)) +
  geom_point(aes(y = age, color = "Age"), size = 3, alpha = 0.7) +
  geom_point(aes(y = poids, color = "Poids"), size = 3, alpha = 0.7) +
  geom_point(aes(y = ifelse(insuffisance_cardiaque == 1, 1, 0), color = "Insuffisance cardiaque"), size = 3, alpha = 0.7) +
  labs(title = "Représentation de l'âge, du poids, du sexe et de l'insuffisance cardiaque en fonction de ddc",
       x = "Année (ddc)", y = "Valeurs") +
  scale_color_manual(values = c("Age" = "salmon", "Poids" = "grey", "Insuffisance cardiaque" = "green")) +
  theme_minimal()




# Graphique de l'âge en fonction des années
graphique_age_evolution <- ggplot(rea_ped_medicament_mesure, aes(x = as.factor(ddc), y = age_annee, group = 1)) +
  geom_line(color = "green") +
  labs(title = "Évolution de l'âge des patients par année",
       x = "Année",
       y = "Âge (années)") +
  theme_minimal()


print(graphique_age_evolution)




# Graphique du poids des patients en fonction des années
graphique_poids_evolution <- ggplot(rea_ped_medicament_mesure, aes(x = as.factor(ddc), y = poids, group = 1)) +
  geom_line(color = "blue") +
  labs(title = "Évolution du poids des patients par année",
       x = "Année",
       y = "Poids") +
  theme_minimal()


print(graphique_poids_evolution)


# Graphique
graphique_substances <- ggplot(rea_ped_medicament_mesure, aes(x = as.factor(ddc), y = administrations, fill = substance)) +
  geom_bar(stat = "identity", position = "dodge") +
  labs(title = "Nombre d'administrations par année",
       x = "Année",
       y = "Nombre d'administrations") +
  scale_fill_manual(values = c("nad" = "blue", "adre" = "green", "milri" = "red", "lasilix" = "purple",
                               "transfu_cgr" = "orange", "transfu_plq" = "yellow", "transfu_pfc" = "pink",
                               "transfu_fibri" = "brown"),
                    name = "Substances") +
  theme_minimal()

# Afficher le graphique
print(graphique_substances)



