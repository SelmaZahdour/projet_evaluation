#### ZAHDOUR Selma
# projet évaluation


##### création table patient

drop table if exists patient;

CREATE TABLE patient (
	ID	int not null,
	sexe char(1) null,
	poids varchar,
	DDN date not null,
	constraint pk_patient PRIMARY key (ID)
	);
	

select * from patient


####création table bloc_rea

drop table if exists bloc_rea

create table bloc_rea (
	ID int not null,
	id_bloc_rea INT not null,
	DDC date not null,
	SAT_PO char(3),
	CEC INT null,
	CLAMPAGE INT null,
	CARDIOPLEGIE INT null,
	TEMP_CEC INT null,
	VENTIL INT null,
	constraint pk_bloc_rea primary key (id_bloc_rea)
	);

select * from bloc_rea


#### création table mesure_post_op

drop table if exists mesure_post_op

CREATE TABLE mesure_post_op (
    id_mesure_post_op INT NOT NULL,
    ID INT NOT NULL,  -- Utilisation de la colonne ID existante
    PH_A INT NOT NULL,
    PCO2_A FLOAT NULL,
    PO2_A FLOAT NULL,
    LACTATES_A FLOAT NULL,
    PH_V INT NOT NULL,
    PCO2_V FLOAT NULL,
    LACTATES_V FLOAT NULL,
    SCVO2 FLOAT NULL,
    PVC FLOAT NULL,
    temp FLOAT NULL,
    tropo FLOAT NULL,
    BNP FLOAT NULL,
    HB FLOAT NULL,
    PLQ FLOAT NULL,
    FIBRI FLOAT NULL,
    CONSTRAINT pk_mesure_post_op PRIMARY KEY (id_mesure_post_op),
    CONSTRAINT fk_mesure_post_op_patient FOREIGN KEY (ID) REFERENCES patient (ID)
);

select * from mesure_post_op
	

####création table adm_medicament

drop table if exists adm_medicament

CREATE TABLE adm_medicament (
    ID_adm_medicament INT NOT NULL,
    NAD FLOAT NULL,
    ADRE FLOAT NULL,
    MILRI FLOAT NULL,
    LASILIX INT NULL,
    TRANSFU_CGR INT NULL,
    TRANSFU_PLQ INT NULL,
    TRANSFU_PFC INT NULL,
    TRANSFU_FIBRI INT NULL,
    CONSTRAINT pk_adm_medicament PRIMARY KEY (ID_adm_medicament),
    CONSTRAINT fk_adm_medicament_patient FOREIGN KEY (ID_adm_medicament) REFERENCES patient (ID)
);

select * from adm_medicament


3)a) SELECT sexe, COUNT(*) AS nombre_de_patients
FROM patient
GROUP BY sexe;

b) SELECT
    EXTRACT(YEAR FROM date_admission) AS annee,
    COUNT(*) AS nombre_de_patients,
    AVG(duree_CEC) AS duree_moyenne_CEC,
    STDDEV(duree_CEC) AS ecart_type_duree_CEC,
    AVG(saturation) AS saturation_moyenne
FROM
    patient
GROUP BY
    annee;
   
   c) select ID,
    MIN(PH_V) AS ph_veineux_min,
    MAX(PH_V) AS ph_veineux_max
FROM
    mesure_post_op
GROUP BY
    ID;

   d) SELECT
    COUNT(DISTINCT CASE WHEN NAD IS NOT NULL OR ADRE IS NOT NULL THEN ID_adm_medicament END) AS nombre_de_patients_norad_adre
FROM
    adm_medicament;

   e) SELECT
    COUNT(DISTINCT ID_adm_medicament) AS nombre_de_patients_sans_norad_adre
FROM
    adm_medicament
WHERE
    NAD IS NULL AND ADRE IS NULL;

